<p align="center">INTEGRACIÓN SALUDABLE</p>

Sistema para la generación de guias nutricionales

## Instalacion

- Crear la base de datos

````.env.example to .env``` 

- Agregar en el .env los parametros de la base de datos usuario y contraseña

````php artisan key:generate``` 

````composer install``` 

````npm install``` 

````php artisan migrate``` 

````php artisan db:seed``` 

- Insertar los roles y permisos

````php artisan db:seed --class=RolesAndPermissionsSeeder``` 

````npm run dev``` 

````php artisan serve``` 

## Acceso Default
- user: admin@admin.com
- password: 12345a
