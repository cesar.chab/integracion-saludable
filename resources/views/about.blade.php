@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' => 'Acerca de nosotros'])

@section('content')
<div class="container" style="height: auto;">
    <div class="row align-items-center">
        <div class="col-lg-12 col-md-12 col-sm-12 ml-auto mr-auto">
            <div class="card_ card-login_ card-hidden_ mb-3_">
                <div class="card-body">
                    <h1 class="card-description_ text-center">Quienes somos</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipiscing elit accumsan, mauris nulla tellus sagittis erat platea. Torquent magnis iaculis tristique arcu cursus nascetur massa, semper nam pretium turpis habitasse sagittis condimentum tellus, tortor curae gravida interdum lacinia penatibus. Bibendum maecenas posuere sem potenti mattis dui justo enim conubia fames ultricies tincidunt, aliquet orci elementum tempor senectus ridiculus gravida nullam morbi vestibulum praesent.

                        Torquent per primis blandit vulputate mus ante conubia placerat metus nam, sapien vel netus dictumst lacinia felis mollis phasellus velit mauris praesent, feugiat cras justo pulvinar suscipit class sociis mattis enim. Nam ante hac felis imperdiet urna tristique condimentum magna sociis fames netus in potenti lacus, varius habitasse quisque pharetra pellentesque orci arcu lectus gravida tellus semper nullam et, suspendisse luctus aliquam iaculis praesent dui curabitur consequat metus class cum proin dignissim. Mattis auctor tempus malesuada venenatis pharetra, elementum quis suspendisse litora montes, cras cubilia hac fusce.</p>
                </div>               
            </div>
        </div>
    </div>
</div>
@endsection