<footer class="footer">
    <div class="container">
        <nav class="float-left">
        <ul>            
            <li>
            <a href="{{route('about')}}">
                Acerca de nosotros
            </a>
            </li>            
            <li>
            <a href="#">
                Licencias
            </a>
            </li>
        </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date('2021-01-19').getFullYear())
        </script>, made by
        <a href="https://www.cesarchabuluac.com" target="_blank">Cesar Chab</a>
        </div>
    </div>
</footer>