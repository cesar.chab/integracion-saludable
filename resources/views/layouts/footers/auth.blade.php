<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
      <ul>
        <li>
          <a href="{{route('about')}}">
            Sobre nosotros
          </a>
        </li>
        <li>
          <a href="#">
            Licencias
          </a>
        </li>
      </ul>
    </nav>
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date('2021-01-19').getFullYear())
      </script>, made by
      <a href="https://cesarchabuluac.com" target="_blank">Cesar Chab</a>
    </div>
  </div>
</footer>